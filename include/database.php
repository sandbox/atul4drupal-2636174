<?php
/**
 * @file
 * This file contains all database queries, that possibly could be used.
 */

/**
 * Implements class to wrap all database fuctions.
 */
class Dbquery {

  // Variable to hold class instance.
  private static $instance;

  /**
   * Implements class construcor.
   */
  private function __construct() {}

  /**
   * Suppress cloning of the class.
   */
  public function __clone() {
    trigger_error('Clone is not allowed.', E_USER_ERROR);
  }

  /**
   * Function to get class instance.
   */
  public static function dbInstance() {
    if (!isset(self::$instance)) {
      $c = __CLASS__;
      self::$instance = new $c();
    }
    return self::$instance;
  }

  /**
   * Function to perform DB Insert operations.
   *
   * @param string $tbl
   *   $tbl nameName of table.
   * @param array $flds
   *   Associative Array $flds, 'key = field name, value = field value'
   *   $flds('type' => 'page', 'uid' => 10).
   * @param int $duplicate
   *   Tells if duplicate entry allowed (optional).
   */
  public function dbInsertQuery($tbl, $flds = array(), $duplicate = 1) {
    if (!isset($tbl) || count($flds) < 1) {
      return FALSE;
    }
    // If duplicate entry not allowed.
    if (!$duplicate) {
      $is_duplicate = $this->dbCheckDuplicateRecord($tbl, $flds);
      if ($is_duplicate) {
        return FALSE;
      }
    }

    $qry = db_insert($tbl);
    $qry->fields($flds);

    try {
      $qry->execute();
    }
    catch (Exception $e) {
      $err_msg = 'Caught exception @dbInsertQuery: ';
      $err_msg .= $e->getMessage() . (int) $e->getCode();
      $this->dbWatchdog($err_msg);
    }
    return TRUE;
  }

  /**
   * Function to perform Db update operation.
   *
   * @param string $tbl
   *   Table name.
   * @param array $flds
   *   Associative array of fields 'key = field name,
   *   value = field value'.
   * @param array $cnd_flds
   *   $cnd_flds associative array of fields 'Key = filed name,
   *   value = field value' to generate record selection condition.
   *
   * @return bool
   *   1 if pass, 0 if fail.
   */
  public function dbUpdateQuery($tbl, $flds = array(), $cnd_flds = array()) {
    if (!isset($tbl) || count($flds) < 1 || count($cnd_flds) < 1) {
      return FALSE;
    }

    $qry = db_update("$tbl");
    $qry->fields($flds);
    foreach ($cnd_flds as $k => $v) {
      $qry->condition("$k", $v);
    }

    try {
      $rs = $qry->execute();
      if ($rs) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    catch (Exception $e) {
      $err_msg = 'Caught exception @dbUpdateQuery: ';
      $err_msg .= $e->getMessage() . (int) $e->getCode();
      $this->dbWatchdog($err_msg);
    }
  }

  /**
   * Function to check if duplicate record exists.
   *
   * @param string $tbl
   *   Table name.
   * @param array $flds
   *   Associative array of fields 'key = field name, value = field value'.
   *
   * @return bool
   *   Boolean 1 if duplicate, 0 if original.
   */
  public function dbCheckDuplicateRecord($tbl, $flds = array()) {
    if (!isset($tbl) || count($flds) < 1) {
      return FALSE;
    }
    $field_names = array_keys($flds);
    $qry = db_select("$tbl", 'tbl');
    $qry->fields("tbl", $field_names);
    foreach ($flds as $k => $v) {
      $qry->condition("tbl.$k", $v);
    }
    $rs = $qry->execute();
    $count = $rs->rowCount();
    if ($count) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Function to get value of a column/field for a given record.
   *
   * @param string $tbl
   *   Table name.
   * @param string $id_fld
   *   Name of the field whose value is to be selected.
   * @param array $condition_flds
   *   Associative array of fields used to build the condition to select
   *   the row.
   *
   * @return array
   *   Array/String, array if multiple values found else the single
   *   value is returned.
   */
  public function dbGetFieldValue($tbl, $id_fld = '', $condition_flds = array()) {
    if ($tbl == '') {
      return FALSE;
    }
    if ($id_fld == '') {
      $id_fld = 'id';
    }
    $qry = db_select("$tbl", 'tbl');
    $qry->fields('tbl', array("$id_fld"));
    foreach ($condition_flds as $k => $v) {
      $qry->condition("tbl.$k", $v);
    }

    $rs = $qry->execute();
    $count = $rs->rowCount();
    $ans = array();
    if ($count > 1) {
      while ($obj = $rs->fetchObject()) {
        $ans[] = $obj->$id_fld;
      }
      return $ans;
    }
    else {
      if ($count) {
        $obj = $rs->fetchObject();
        return $obj->$id_fld;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Implements function to get MAX value record.
   *
   * @param string $tbl
   *   Table name.
   * @param string $max_fld
   *   Field for which max value required.
   * @param array $condition_flds
   *   Associative array $condition_flds associative array of fields
   *   'Key = filed name, value = field value' to generate
   *   record selection condition.
   *
   * @return value
   *   The value if found, 0 if value not found.
   */
  public function dbGetMaxRecord($tbl, $max_fld, $condition_flds = array()) {
    if ($tbl == '' || $max_fld == '') {
      return FALSE;
    }

    $qry = db_select($tbl, 'tbl');
    foreach ($condition_flds as $k => $v) {
      $qry->condition("tbl.$k", $v);
    }
    $qry->addExpression("MAX($max_fld)", 'max_val');
    $rs = $qry->execute();
    $count = $rs->rowCount();
    if ($count) {
      $obj = $rs->fetchObject();
      return $obj->max_val;
    }
    else {
      return 0;
    }
  }

  /**
   * Implements function to get MIN value record.
   *
   * @param string $tbl
   *   Table name.
   * @param string $min_fld
   *   Field for which min value required.
   * @param array $condition_flds
   *   Associative array $condition_flds associative array of fields
   *   'Key = filed name, value = field value'to generate
   *   record selection condition.
   *
   * @return value
   *   The value if found, 0 if value not found.
   */
  public function dbGetMinRecord($tbl, $min_fld, $condition_flds = array()) {
    if ($tbl == '' || $min_fld == '') {
      return FALSE;
    }

    $qry = db_select($tbl, 'tbl');
    foreach ($condition_flds as $k => $v) {
      $qry->condition("tbl.$k", $v);
    }
    $qry->addExpression("MIN($min_fld)", 'min_val');
    $rs = $qry->execute();
    $count = $rs->rowCount();
    if ($count) {
      $obj = $rs->fetchObject();
      return $obj->min_val;
    }
    else {
      return 0;
    }
  }

  /**
   * Implements a function to select all records meeting a condition.
   *
   * @param string $tbl
   *   String $tbl table name.
   * @param array $flds_to_select
   *   Array $flds_to_select palin array of fields that are to be selected.
   * @param array $condition_flds
   *   Array $condition_flds associative array of conditional fields.
   * @param string $operator
   *   String $operator operator to use in query condition, defaults to =.
   * @param array $limit
   *   Array $limit if limit to be used, gives start and end range, 1st
   *   element is start point.
   *
   * @return array
   *   Array associative array with fields and there respective values.
   */
  public function dbConditionalSelect($tbl, $flds_to_select = array(), $condition_flds = array(), $operator = NULL, $limit = array()) {
    if ($tbl == '' || !count($condition_flds) || !count($flds_to_select)) {
      return FALSE;
    }
    $qry = db_select("$tbl", 'tbl');
    $qry->fields('tbl', $flds_to_select);
    foreach ($condition_flds as $k => $v) {
      if (is_null($operator)) {
        if (is_array($v)) {
          $fld_val = $v[0];
          $fld_exp = $v[1];
          $qry->condition("tbl.$k", $fld_val, $fld_exp);
        }
        else {
          $qry->condition("tbl.$k", $v);
        }
      }
      else {
        if (is_array($v)) {
          $fld_val = $v[0];
          $fld_exp = $v[1];
          $qry->condition("tbl.$k", $fld_val, $fld_exp);
        }
        else {
          $qry->condition("tbl.$k", $v, $operator);
        }
      }
    }
    if (count($limit)) {
      $strt = $limit[0];
      $end = $limit[1];
      $qry->range($strt, $end);
    }
    $rs = $qry->execute();

    $count = $rs->rowCount();
    $ans = array();
    if ($count) {
      $i = 0;
      while ($obj = $rs->fetchObject()) {
        $record = array();
        foreach ($flds_to_select as $fld) {
          $record[$fld] = $obj->$fld;
        }
        $ans[$i++] = $record;
      }
    }
    return $ans;
  }

  /**
   * Implements function to return all vocabulary tree.
   *
   * @param string $vocab_name
   *   String $vocab_name vocabulary name whose tree is to be
   *   selected (not machine name).
   *
   * @return array
   *   Associative Array $all_terms if found, 0 if no result found.
   */
  public function dbGetVocabTreeByName($vocab_name = '') {
    if ($vocab_name == '') {
      return FALSE;
    }
    $vocab_name = trim($vocab_name);
    // First get vocabulary ID.
    $vocab_vid = '';
    $vocab = taxonomy_get_vocabularies();
    foreach ($vocab as $v) {
      if ($v->name == $vocab_name) {
        $vocab_vid = $v->vid;
        break;
      }
    }

    if ($vocab_vid != '') {
      $terms = taxonomy_get_tree($vocab_vid);
      $all_terms = array();
      foreach ($terms as $v) {
        $all_terms[$v->tid] = $v->name;
      }
      return $all_terms;
    }
    return 0;
  }

  /**
   * Implements delete query.
   *
   * @param string $tbl
   *   Table name.
   * @param array $condition_flds
   *   Associative array $condition_flds associative array of fields
   *   'Key = filed name, value = field value' to generate
   *   record selection condition.
   */
  public function dbDeleteQuery($tbl, $condition_flds = array()) {
    if ($tbl == '' || !count($condition_flds)) {
      return FALSE;
    }
    $qry = db_delete($tbl);
    foreach ($condition_flds as $k => $v) {
      $qry->condition("'$k'", "'$v'");
    }

    try {
      $qry->execute();
      return TRUE;
    }
    catch (Exception $e) {
      $err_msg = 'Caught exception @dbDeleteQuery: ';
      $err_msg .= $e->getMessage() . (int) $e->getCode();
      $this->dbWatchdog($err_msg);
    }
  }

  /**
   * Function to fetch all records.
   *
   * @param string $tbl
   *   String $tbl table name.
   */
  public function dbSelectAll($tbl) {
    $qry = db_select("$tbl", 'tbl');
    $rs = $qry->execute();
    return $rs;
  }

  /**
   * Function to return count of records meeting a condition.
   *
   * @param string $tbl
   *   String $tbl table name.
   * @param array $fld_name
   *   Array $fld_name plain array of fields that are to be selected.
   * @param array $condition_flds
   *   Array $condition_flds associative array of conditional fields.
   *
   * @return int
   *   Number $count count of the records.
   */
  public function dbConditionalRecordCount($tbl, $fld_name = array(), $condition_flds = array()) {
    if ($tbl == '' || !count($condition_flds)) {
      return FALSE;
    }
    $qry = db_select("$tbl", 'tbl');
    if ($fld_name != '') {
      $qry->fields('tbl', $fld_name);
    }
    foreach ($condition_flds as $k => $v) {
      $qry->condition("tbl.$k", $v);
    }
    $rs = $qry->execute();
    return $rs->rowCount();
  }

  /**
   * Implemenhts function to execute almost any db_select query.
   *
   * @param string $tbl
   *   String $tbl table name.
   * @param array $fld_to_select
   *   Array $fld_to_select array of fields to select.
   * @param array $con_flds
   *   Array $con_flds associative array of condition fields.
   * @param array $expression
   *   Associative Array of expressions for the query -
   *   "alias" => "expression" ex. all" => "COUNT('entity_id')".
   * @param string $groupby
   *   String $groupby groupby clause.
   * @param string $orderby
   *   String $orderby orderBy clause.
   * @param string $orderas
   *   String $orderas how to order result in ASC or DESC.
   * @param array $limit
   *   Array $limit adds limit to query 1 element is first argument.
   *
   * @return array
   *   Array $result associative array.
   */
  public function dbSelectWithExpression($tbl, $fld_to_select = array(), $con_flds = array(), $expression = array(), $groupby = '', $orderby = '', $orderas = 'ASC', $limit = array()) {
    if ($tbl == '' || !count($con_flds) || !count($fld_to_select)) {
      return 0;
    }
    $result = array();

    $qry = db_select($tbl, 'tbl');
    $qry->fields('tbl', $fld_to_select);

    foreach ($con_flds as $k => $v) {
      if (is_array($v)) {
        $fld_val = $v[0];
        $fld_exp = $v[1];
        $qry->condition("tbl.$k", $fld_val, $fld_exp);
      }
      else {
        $qry->condition("tbl.$k", $v);
      }
    }

    if (count($expression)) {
      foreach ($expression as $alias => $exp) {
        $qry->addExpression("$exp", "$alias");
      }
    }

    if ($groupby != '') {
      $qry->groupBy("$groupby");
    }

    if ($orderby != '') {
      $qry->orderBy("$orderby", $orderas);
    }

    if (count($limit)) {
      $strt = $limit[0];
      $end = $limit[1];
      $qry->range($strt, $end);
    }

    $rs = $qry->execute();

    if ($rs->rowCount()) {
      $i = 0;
      while ($obj = $rs->fetchObject()) {
        $tmp = array();
        foreach ($fld_to_select as $v) {
          $tmp[$v] = $obj->$v;
        }
        if ($expression) {
          $tmp['expression'] = $obj->expression;
        }
        $result[$i++] = $tmp;
      }
    }
    return $result;
  }

  /**
   * Implements function to execute select query with join.
   *
   * @param string $tbl
   *   Table name.
   * @param array $joins
   *   Associative Array $join
   *   $joins[] = array(
   *     'tbl' => 'Table Name',
   *     'alias' => 'Tbl Alias',
   *     'on' => "alias.field = alias.field",
   *     'fields' => array('field1', 'field2'),
   *     'condition' => array('n.type' => 'badge') or
   *     array('n.nid' => array(100, '>='))
   *   );.
   * @param array $flds_to_select
   *   Array $flds_to_select (fields from primary table).
   * @param array $conditions
   *   Associative Array $conditions condition array.
   * @param string $join_name
   *   String $join_name, name of join to use 'leftjoin' or 'rightjoin'.
   *
   * @return object
   *   Object $rs mysql resultset.
   */
  public function dbSelectWithJoin($tbl, $joins = array(), $flds_to_select = array(), $conditions = array(), $join_name = 'join') {
    if ($tbl == '' || count($joins) == 0) {
      return 0;
    }

    $join_fields_ary = array();
    $join_condition_ary = array();
    $qry = db_select($tbl, 'tbl');
    // Loop joins array.
    foreach ($joins as $join_ary) {
      $join_tbl = $join_ary['tbl'];
      $join_tbl_alias = $join_ary['alias'];
      $join_on = $join_ary['on'];
      if (array_key_exists('fields', $join_ary)) {
        $join_fields_ary[] = array($join_tbl_alias, $join_ary['fields']);
      }
      if (array_key_exists('condition', $join_ary)) {
        $join_condition_ary[] = $join_ary['condition'];
      }
      switch ($join_name) {
        case 'join':
          $qry->join("$join_tbl", "$join_tbl_alias", "$join_on");
          break;

        case 'leftjoin':
          $qry->leftJoin("$join_tbl", "$join_tbl_alias", "$join_on");
          break;

        case 'rightjoin':
          $qry->rightJoin("$join_tbl", "$join_tbl_alias", "$join_on");
          break;
      }
    }

    // Loop join fields.
    foreach ($join_fields_ary as $val) {
      $tbl_alias = $val[0];
      $flds = $val[1];
      $qry->fields($tbl_alias, $flds);
    }

    // Add fields from primary table.
    if (count($flds_to_select)) {
      $qry->fields('tbl', $flds_to_select);
    }

    // Include join conditions.
    if (count($join_condition_ary)) {
      foreach ($join_condition_ary as $val) {
        foreach ($val as $k => $v) {
          if (is_array($v)) {
            $fld_val = $v[0];
            $fld_exp = $v[1];
            $qry->condition($k, $fld_val, $fld_exp);
          }
          else {
            $qry->condition($k, $v);
          }
        }
      }
    }

    // Add primary conditions.
    if (count($conditions)) {
      foreach ($conditions as $k => $v) {
        if (is_array($v)) {
          $fld_val = $v[0];
          $fld_exp = $v[1];
          $qry->condition("tbl.$k", $fld_val, $fld_exp);
        }
        else {
          $qry->condition("tbl.$k", $v);
        }
      }
    }

    try {
      $rs = $qry->execute();
      return $rs;
    }
    catch (Exception $e) {
      $err_msg = 'Caught exception @dbSelectWithJoin: ';
      $err_msg .= $e->getMessage() . (int) $e->getCode();
      $this->dbWatchdog($err_msg);
    }
  }

  /**
   * Implements watchdog entry function.
   */
  public function dbWatchdog($msg, $type = WATCHDOG_ERROR) {
    watchdog('dbquery', $msg, array(), $type, NULL);
  }

}
