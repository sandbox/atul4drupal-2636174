Here you will find examples for all the queries that this module supports, 
these were defined in include/database.php file

Note: Its important to get reference of the DB class before calling any
function of the class
* $db variable will hold class instance
$db = Dbquery::dbInstance();

* DB INSERT QUERY
$db = Dbquery::dbInstance();
$tbl = 'node';
$flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbInsertQuery($tbl, $flds);


* DB UPDATE QUERY
$db = Dbquery::dbInstance();
$tbl = 'node';
$flds = array(
  'type' => 'page',
);
$cnd_flds = array(
  'uid' => 10,
);
$db->dbUpdateQuery($tbl, $flds, $cnd_flds);


* DB QUERY TO CHECK DUPLICATE ENTRY
$db = Dbquery::dbInstance();
$tbl = 'node';
$flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbCheckDuplicateRecord($tbl, $flds);  * this will check if any
record with uid 10 and type page exists in node table


* DB QUERY TO GET SINGLE FIELD VALUE(s)
$db = Dbquery::dbInstance();
$tbl = 'node';
$id_fld = 'nid';
$cnd_flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbGetFieldValue($tbl, $id_fld, $cnd_flds);


* DB QUERY TO GET MAX RECORD
$db = Dbquery::dbInstance();
$tbl = 'node';
$max_fld = 'nid';
$cnd_flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbGetMaxRecord($tbl, $max_fld, $cnd_flds);


* DB QUERY TO GET MIN RECORD
$db = Dbquery::dbInstance();
$tbl = 'node';
$min_fld = 'nid';
$cnd_flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbGetMinRecord($tbl, $min_fld, $cnd_flds);


* DB QUERY FOR CONDITIONAL RECORD SELECT
$db = Dbquery::dbInstance();
$tbl = 'node';
$flds = array('nid', 'uid');
$cnd_flds = array(
  'type' => 'page',
);
$operator = '!='; * can be 'NULL' to use default '=' oerator
$limit(1, 10);
$db->dbConditionalSelect($tbl, $flds, $cnd_flds, $operator, $limit);


* DB QUERY TO GET VOCABULARY TREE BY VOCAB. NAME (Don't use machine name)
$db = Dbquery::dbInstance();
$vocab_name = 'Tag';
$db->dbGetVocabTreeByName($vocab_name);


* DB DELETE QUERY
$db = Dbquery::dbInstance();
$tbl = 'node';
$cnd_flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbDeleteQuery($tbl, $cnd_flds);


* DB QUERY TO GET ALL RECORDS
$db = Dbquery::dbInstance();
$tbl = 'node';
$db->dbSelectAll($tbl);


* DB QUERY TO GET RECORD COUNT BASED ON CONDITION
$db = Dbquery::dbInstance();
$tbl = 'node';
$flds = array('nid');
$cnd_flds = array(
  'type' => 'page',
  'uid' => 10
);
$db->dbConditionalRecordCount($tbl, $flds, $cnd_flds);


* DB QUERY TO SELECT RECORDS WITH ALMOST ALL CLAUSES OF SELECT QUERY
$db = Dbquery::dbInstance();
$tbl = 'node';
$flds = array('nid');
$cnd_flds = array(
  'type' => 'page',
);
$expression = array('all' => "COUNT('nid')");
$groupby = 'uid';
$orderby = 'uid';
$orderas = 'DESC';
$limit = array(0, 10);
$db->dbSelectWithExpression($tbl, $flds, $cnd_flds, $expression,
$groupby, $orderby, $orderas, $limit);

* DB QUERY WITH JOIN
$tbl = 'node'; // for the primary table, module by default uses 'tbl' prefix
$joins[] = array(
  'tbl' => 'users',
  'alias' => 'usr',
  'on' => "usr.uid = tbl.uid",
  'fields' => array('name', 'mail'),
  'condition' => array('usr.uid' => array(array(1,2,5,7), 'IN'));
);
$flds = array('nid', 'type');
$cnd_flds = array('type' => 'article');
$join_name = 'leftjoin';
dbSelectWithJoin($tbl, $joins, $flds, $cnd_flds, $join_name);
